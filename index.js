require("dotenv").config();
const express = require("express");
const jwt = require("jsonwebtoken");
const util = require("util");
const jwtSignAsync = util.promisify(jwt.sign);
const bcrypt = require("bcrypt");
const morgan = require("morgan");

const app = express();
const PORT = process.env.PORT || 3000;
const { User, Game } = require("./models/index");
const passport = require("./utils/passport");

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  require("express-session")({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.set("view engine", "ejs");
app.use(morgan("dev"));

app.post("/login", async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res.json({
      errors: "Missing credentials",
    });
  }

  const user = await User.findOne({
    where: {
      userName: username,
    },
  });

  if (!user || !(await bcrypt.compare(password, user.password))) {
    return res.status(404).json({
      error: "Invalid credentials",
    });
  }
  const token = await jwtSignAsync(
    {
      sub: user.id,
      iss: "game-app-binar",
      aud: "dashboard",
    },
    process.env.JWT_SECRET,
    {
      expiresIn: "1h",
    }
  );

  res.json(token);
});

app.get("/games", async (req, res) => {
  const games = await Game.findAll();
  res.json({
    data: games,
  });
});

// web
app.get("/dashboard/login", (req, res) => {
  res.render("login");
});

app.get("/dashboard/home",(req, res) => {
  console.log(req.session);
  res.render("index");
});

app.post(
  "/dashboard/login",
  passport.authenticate("local", {
    successRedirect: "/dashboard/home",
    failureRedirect: "",
  })
);

app.listen(PORT, () => {
  console.log("Server listening on %s:%d", process.env.HOST, PORT);
});
