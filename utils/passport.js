const passport = require("passport");
const localStrategy = require("passport-local").Strategy;
const { User } = require("../models/index");
const bcrypt = require("bcrypt");

passport.use(
  new localStrategy(
    {
      usernameField: "username",
      passwordField: "password",
    },
    async function (username, password, done) {
      try {
        const user = await User.findOne({
          where: {
            userName: username,
          },
        });

        if(!user || !await bcrypt.compare(password, user.password)) return done('Invalid credentials', false);

        return done(null, user);
      } catch (error) {
        done(error);
      }
    }
  )
);

passport.serializeUser(function(user, done) {
    done(null, user.id);
});
  
passport.deserializeUser(async function(id, done) {
    try {
        const user = await User.findByPk(id);
        done(null, user);
    } catch (error) {
        done(error);
    }
});

module.exports = passport;
