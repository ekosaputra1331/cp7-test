'use strict';
const bcrypt = require('bcrypt');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return await queryInterface.bulkInsert('users', [{
      firstName: 'John',
      lastName: 'Doe',
      email: 'example@example.com',
      userName: 'jhon123',
      password: await bcrypt.hash('admin123', 10),
      role: 'player',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      firstName: 'Eko',
      lastName: 'Saputra',
      userName: 'ekosaputra123',
      password: await bcrypt.hash('admin123', 10),
      role: 'player',
      email: 'eko@example.com',
      createdAt: new Date(),
      updatedAt: new Date()
    }
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete('users', null, {});
  }
};
