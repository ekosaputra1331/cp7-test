'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    return await queryInterface.bulkInsert('games', [{
      name: 'Rock-Paper-Scissor',
      description: "ini adalah game traditional",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Gasing',
      description: "gasing adalah game traditional",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Layangan',
      description: "layangan adalah game traditional",
      createdAt: new Date(),
      updatedAt: new Date()
    }
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('games', null, {});
  }
};
